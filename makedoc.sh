#! /usr/bin/env bash

# Create document of each crate listed in ~/.cargo/doctool/crates.txt

fatal() {
  echo "$0: $*" >&2
  exit 1
}

main() {
  local crate  # name@version
  local cratev # name:version

  mkdir -p ~/.cargo/doctool
  if ! cd ~/.cargo/doctool; then
    fatal "failed to goto doctool workdir"
  fi

  cargo init 2>/dev/null

  while read -r crate; do
    echo "Add crate \"$crate\""

    if ! cargo add "$crate"; then
      fatal "Can't add crate $crate"
    fi

    cratev="${crate//@/:}"
    echo "Create document of \"$cratev\""

    if ! cargo doc --all-features \
      --target-dir=. --package="$cratev" --no-deps; then
          fatal "Can't create document of $crate"
    fi
  done < crates.txt
}

main
