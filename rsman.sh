#! /usr/bin/env bash

exec rusty-man -s ~/.cargo/doctool/doc/ "$@"
